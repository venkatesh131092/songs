import {combineReducers} from 'redux';
const songReducer=()=>{
    return [
            {title:'No Scrubs',duration:'4:05'},
            {title:'Macarena',duration:'2:30'},
            {title:'All stal',duration:'3:15'},
            {title:'I want it now',duration:'4:35'}
    ];
};
const selectedSongReducer=(selectedSong=null,action)=>{
    if(action.type==='SONG_SELECTED'){
        return action.payload;
    }
    return selectedSong;
};

export default combineReducers({
    songs:songReducer,
    selectedSong:selectedSongReducer
});